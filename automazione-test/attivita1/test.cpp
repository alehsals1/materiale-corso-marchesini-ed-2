#define CATCH_CONFIG_MAIN

#include "catch.hpp"

TEST_CASE("Asserzione vera") {
  REQUIRE(1 == 1);
}

TEST_CASE("Asserzione falsa") {
  REQUIRE(1 == 2);
}