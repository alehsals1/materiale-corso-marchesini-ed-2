#define CATCH_CONFIG_MAIN

#include "../catch.hpp"
#include "operatori.h"

TEST_CASE("sequenze di due operazioni") {
  int iniziale = GENERATE(take(100,random(-10000,10000)));
  int a = iniziale;
  SECTION("incremento") {
    inc(&a);
    REQUIRE(a == iniziale+1);
    SECTION("incremento") {
      inc(&a);
      REQUIRE(a == iniziale+2);
    }
    SECTION("decremento") {
      dec(&a);
      REQUIRE(a == iniziale);
    }
  }
  SECTION("decremento") {
    dec(&a);
    REQUIRE(a == iniziale-1);
    SECTION("incremento") {
      inc(&a);
      REQUIRE(a == iniziale);
    }
    SECTION("decremento") {
      dec(&a);
      REQUIRE(a == iniziale -2);
    }
  }
}
